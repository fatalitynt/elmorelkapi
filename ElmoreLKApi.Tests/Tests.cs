using System.IO;
using System.Linq;
using ElmoreLKApi.DAL;
using NUnit.Framework;

namespace ElmoreLKApi.Tests;

public class Tests
{

    [Test]
    public void FakeTestToInsertData()
    {
        var itemsToInsert = File.ReadAllLines(@"C:\Users\Fatality\Desktop\manor_data_n.txt")
            .Skip(1)
            .Select(ManorNextDto.Parse)
            .ToArray();

        using var db = new ManorContext();
        db.ManorNextDtos.RemoveRange(db.ManorNextDtos);
        db.ManorNextDtos.AddRange(itemsToInsert);
        db.SaveChanges();
    }
}