﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElmoreLKApi.DAL;

[Table("manor_data")]
public class ManorDto
{
    [Key]
    public int id { get; set; }
    public int manor_id { get; set; }
    public int data_index { get; set; }
    public int seed_id { get; set; }
    public int seed_price { get; set; }
    public int seed_sell_count { get; set; }
    public int seed_remain_count { get; set; }
    public int crop_id { get; set; }
    public int crop_buy_count { get; set; }
    public int crop_price { get; set; }
    public int crop_type { get; set; }
    public int crop_remain_count { get; set; }
    public int crop_deposit { get; set; }

    [Obsolete("DO_NOT_USE: for manual data inject")]
    public static ManorDto Parse(string s)
    {
        var parts = s.Split("\t").Select(int.Parse).ToArray();
        return new ManorDto
        {
            id = Guid.NewGuid().GetHashCode(),
            manor_id = parts[0],
            data_index = parts[1],
            seed_id = parts[2],
            seed_price = parts[3],
            seed_sell_count = parts[4],
            seed_remain_count = parts[5],
            crop_id = parts[6],
            crop_buy_count = parts[7],
            crop_price = parts[8],
            crop_type = parts[9],
            crop_remain_count = parts[10],
            crop_deposit = parts[11]
        };
    }
}