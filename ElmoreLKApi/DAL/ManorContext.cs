﻿using Microsoft.EntityFrameworkCore;

namespace ElmoreLKApi.DAL;

public class ManorContext : DbContext
{
    public DbSet<ManorDto> ManorDtos { get; set; }
    public DbSet<ManorNextDto> ManorNextDtos { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlServer(@"Server=localhost\SQLEXPRESS;Database=master;Trusted_Connection=True;");
    }
}