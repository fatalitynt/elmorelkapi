﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElmoreLKApi.DAL;

[Table("manor_data_next")]
public class ManorNextDto
{
    [Key]
    public int id { get; set; }
    public int manor_id { get; set; }
    public int data_index { get; set; }
    public int seed_id_n { get; set; }
    public int seed_price_n { get; set; }
    public int seed_sell_count_n { get; set; }
    public int crop_id_n { get; set; }
    public int crop_buy_count_n { get; set; }
    public int crop_price_n { get; set; }
    public int crop_type_n { get; set; }

    [Obsolete("DO_NOT_USE: for manual data inject")]
    public static ManorNextDto Parse(string s)
    {
        var parts = s.Split("\t").Select(int.Parse).ToArray();
        return new ManorNextDto
        {
            id = Guid.NewGuid().GetHashCode(),
            manor_id = parts[0],
            data_index = parts[1],
            seed_id_n = parts[2],
            seed_price_n = parts[3],
            seed_sell_count_n = parts[4],
            crop_id_n = parts[5],
            crop_buy_count_n = parts[6],
            crop_price_n = parts[7],
            crop_type_n = parts[8],
        };
    }
}