﻿namespace ElmoreLKApi.Models;

public class ManorResponse
{
    public ManorInfo[] ManorInfos { get; set; }
}