using ElmoreLKApi.DAL;

namespace ElmoreLKApi.Models;

public class ManorInfo
{
    public int ManorId { get; set; }
    public SeedInfoLine[] SeedLines { get; set; }
    public CropInfoLine[] CropLines { get; set; }
    public SeedInfoLineNext[] SeedLinesNext { get; set; }
    public CropInfoLineNext[] CropLinesNext { get; set; }

    public ManorInfo(int manorId, IEnumerable<ManorDto> items, IEnumerable<ManorNextDto> itemsNext)
    {
        ManorId = manorId;

        var sortedItems = items.OrderBy(x => x.data_index).ToArray();
        SeedLines = sortedItems.Select(x => new SeedInfoLine(x)).ToArray();
        CropLines = sortedItems.Select(x => new CropInfoLine(x)).ToArray();

        var sortedItemsNext = itemsNext.OrderBy(x => x.data_index).ToArray();
        SeedLinesNext = sortedItemsNext.Select(x => new SeedInfoLineNext(x)).ToArray();
        CropLinesNext = sortedItemsNext.Select(x => new CropInfoLineNext(x)).ToArray();
    }
}