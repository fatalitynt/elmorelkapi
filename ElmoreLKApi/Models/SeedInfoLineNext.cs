﻿using ElmoreLKApi.DAL;

namespace ElmoreLKApi.Models;

public class SeedInfoLineNext
{
    public int SeedId { get; set; }
    public int SeedPrice { get; set; }
    public int SeedSellCount { get; set; }

    public SeedInfoLineNext(ManorNextDto manorDto)
    {
        SeedId = manorDto.seed_id_n;
        SeedPrice = manorDto.seed_price_n;
        SeedSellCount = manorDto.seed_sell_count_n;
    }
}