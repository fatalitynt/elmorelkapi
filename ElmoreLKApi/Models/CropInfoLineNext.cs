﻿using ElmoreLKApi.DAL;

namespace ElmoreLKApi.Models;

public class CropInfoLineNext
{
    public int CropId { get; set; }
    public int CropBuyCount { get; set; }
    public int CropPrice { get; set; }
    public int CropType { get; set; }

    public CropInfoLineNext(ManorNextDto manorDto)
    {
        CropId = manorDto.crop_id_n;
        CropBuyCount = manorDto.crop_buy_count_n;
        CropPrice = manorDto.crop_price_n;
        CropType = manorDto.crop_type_n;
    }
}