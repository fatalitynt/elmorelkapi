﻿using ElmoreLKApi.DAL;

namespace ElmoreLKApi.Models;

public class CropInfoLine
{
    public int CropId { get; set; }
    public int CropBuyCount { get; set; }
    public int CropPrice { get; set; }
    public int CropType { get; set; }
    public int CropRemainCount { get; set; }
    public int CropDeposit { get; set; }

    public CropInfoLine(ManorDto manorDto)
    {
        CropId = manorDto.crop_id;
        CropBuyCount = manorDto.crop_buy_count;
        CropPrice = manorDto.crop_price;
        CropType = manorDto.crop_type;
        CropRemainCount = manorDto.crop_remain_count;
        CropDeposit = manorDto.crop_deposit;
    }
}