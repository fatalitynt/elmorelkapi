﻿using ElmoreLKApi.DAL;

namespace ElmoreLKApi.Models;

public class SeedInfoLine
{
    public int SeedId { get; set; }
    public int SeedPrice { get; set; }
    public int SeedSellCount { get; set; }
    public int SeedRemainCount { get; set; }

    public SeedInfoLine(ManorDto manorDto)
    {
        SeedId = manorDto.seed_id;
        SeedPrice = manorDto.seed_price;
        SeedSellCount = manorDto.seed_sell_count;
        SeedRemainCount = manorDto.seed_remain_count;
    }
}