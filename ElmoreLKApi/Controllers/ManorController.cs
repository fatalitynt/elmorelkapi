using ElmoreLKApi.DAL;
using ElmoreLKApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace ElmoreLKApi.Controllers;

[ApiController]
[Route("[controller]")]
public class ManorController : ControllerBase
{
    [HttpGet(Name = "GetManor")]
    public object Get(bool showAll)
    {
        var todayManorMap = FetchData()
            .Where(x => showAll || x.seed_sell_count + x.crop_buy_count > 0)
            .GroupBy(x => x.manor_id)
            .ToDictionary(x => x.Key, x => x.ToArray());

        var nextDayManorMap = FetchDataForNextDay()
            .Where(x => showAll || x.seed_sell_count_n + x.crop_buy_count_n > 0)
            .GroupBy(x => x.manor_id)
            .ToDictionary(x => x.Key, x => x.ToArray());


        var keys = todayManorMap.Keys.Concat(nextDayManorMap.Keys).Distinct().ToArray();

        var manorInfos = keys
            .Select(x => new ManorInfo(x,
                todayManorMap.TryGetValue(x, out var tod) ? tod : Array.Empty<ManorDto>(),
                nextDayManorMap.TryGetValue(x, out var tom) ? tom : Array.Empty<ManorNextDto>()))
            .OrderBy(x => x.ManorId)
            .ToArray();

        return new ManorResponse { ManorInfos = manorInfos };
    }

    private ManorDto[] FetchData()
    {
        try
        {
            using var db = new ManorContext();
            return db.ManorDtos.ToArray();
        }
        catch (Exception e)
        {
            // LOG ERROR
            return Array.Empty<ManorDto>();
        }
    }

    private ManorNextDto[] FetchDataForNextDay()
    {
        try
        {
            using var db = new ManorContext();
            return db.ManorNextDtos.ToArray();
        }
        catch (Exception e)
        {
            // LOG ERROR
            return Array.Empty<ManorNextDto>();
        }
    }
}
